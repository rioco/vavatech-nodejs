import React from 'react'
import Layout from './Layout.jsx';

export const Pages = (props) => {
    return (
        <Layout {...props}>
            <div className="container">
                <div className="row">
                    <div className="col">
                        <span className="float-end">
                            <h4> ( <span id="count">0</span> ) <span id="total">0</span> PLN</h4>
                        </span>
                        <h1>Products</h1>


                        <form action="/pages/products" method="GET">

                            <div className="input-group mb-3">
                                <input type="search" value={props.filter} className="form-control" name="filter" placeholder="Search products" />
                                <button className="btn btn-outline-secondary" type="submit">Search</button>
                            </div>
                        </form>

                        <div className="list-group">
                            {props.products.map(product =>
                                <div className="list-group-item" key={product.url}>

                                    {product.image && <img src={product.image} width="100" className="me-3" />}

                                    <a href={product.url}>
                                        {product.name}
                                    </a>
                                    <a href="#"
                                        data-product-id={product.id}
                                        className="btn btn-xs btn-info float-end js-add-to-cart">Add to cart</a>
                                </div>
                            )}
                        </div>

                        <a href="/pages/product-form" className="btn btn-info mt-3">Create product</a>
                    </div>
                </div>
            </div>
            <script dangerouslySetInnerHTML={{
                __html: /* javascript */`
                
                function updateCart(cart){
                    total.innerText = cart.total
                    count.innerText = cart.count
                } 
                
                fetch('/cart/').then(res => res.json())
                .then(updateCart)

                document.addEventListener('click', event => {
                    if(!event.target.matches('.js-add-to-cart')){ return; }
                    const product_id = event.target.dataset.productId

                    fetch('/cart/add/',{
                        method:'POST',
                        body: JSON.stringify({ product_id: product_id }),
                        headers:{
                            'Content-Type':'application/json'
                        }
                    }).then(res => res.json())
                    .then(updateCart)

                })
            `}} />
        </Layout>
    )
}

export default Pages;