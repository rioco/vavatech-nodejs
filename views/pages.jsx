import React from 'react'
import Layout from './Layout.jsx';

export const Pages = (props) => {
    return (
        <Layout {...props}>
            <div className="container">
                <div className="row">
                    <div className="col">
                        <h1>Welcome React {props.title}</h1>

                        <div className="list-group">
                            {props.pages.map(page =>
                                <div className="list-group-item" key={page.url}>
                                    <a href={page.url}>
                                        {page.title}
                                    </a>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    )
}

export default Pages;