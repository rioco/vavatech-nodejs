import fs from 'fs'
import path from 'path'


export function generateFakeLogsPromise(filePath: string, n = 1000) {
    return new Promise((resolve, reject) => {

        fs.promises.mkdir(path.dirname(filePath), { recursive: true })
            .then(() => writeNextLog(n))

        function writeNextLog(count: number) {
            const log = (`[${(new Date()).toISOString()}] [Info] - Example log ${~~(Math.random() * 10000)} \n`)

            fs.promises.appendFile(filePath, log)
                .then(() => {
                    // Success
                    if (count == 0) { resolve(count); return }

                    // Continue to next log
                    writeNextLog(count - 1)
                }, err => reject(err))
        }
    })
}