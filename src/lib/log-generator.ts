import fs from 'fs'
import path from 'path'


export function generateFakeLogs(filePath: string, n = 1000, callback: (error?: any, result?: number) => void) {

    fs.mkdir(path.dirname(filePath), { recursive: true }, (err) => {
        if (err) { return console.error(err) }
        writeNextLog(n)
    })


    function writeNextLog(count: number) {
        const log = (`[${(new Date()).toISOString()}] [Info] - Example log ${~~(Math.random() * 10000)} \n`)

        fs.appendFile(filePath, log, (err) => {
            if (err) {
                callback(err); return;
            }
            if (count == 0) { callback(null, count); return }

            writeNextLog(count - 1)
        })
    }
}