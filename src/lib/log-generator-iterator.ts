import fs from 'fs'
import path from 'path'

export function* generateFakeLogsGenerator(filePath: string, n = 1000) {

    yield fs.promises.mkdir(path.dirname(filePath), { recursive: true })

    for (var i = 0; i < n; i++) {
        const log = (`[${(new Date()).toISOString()}] [Info] - Example log ${~~(Math.random() * 10000)} \n`)

        yield fs.promises.appendFile(filePath, log).then(() => i)
    }

    return;
}