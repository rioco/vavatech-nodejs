import fs from 'fs';
import crypto from "crypto";

export function md5(filePath: string) {
    return new Promise((resolve, reject) => {
        const md5hash = crypto.createHash("md5").setEncoding("hex");
        const stream = fs.createReadStream(filePath);
        stream.pipe(md5hash);
        stream.on('error', err => reject(err));
        stream.on("end", () => {
            resolve(md5hash.read());
        });
    });
}
;
