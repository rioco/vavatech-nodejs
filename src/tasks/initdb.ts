import mongoose from 'mongoose'
import { Product } from '../models/product'

mongoose.connect('mongodb://localhost:27017', {
    dbName: 'database',
    useNewUrlParser: true,
    useUnifiedTopology: true
}, (err) => {
    if (err) { console.error(err) }

    Product.insertMany([
        { name: 'Product 123', description: '', price: 100 },
        { name: 'Product 234', description: '', price: 200 },
        { name: 'Product 345', description: '', price: 300 },
    ])
})
