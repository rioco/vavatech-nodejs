import { Document, Model, model, Schema } from "mongoose";

interface IProduct {
    _id: string;
    name: string;
    description: string;
    price: number;
    image?: string
}


export const productSchema = new Schema<IProduct>({
    name: {
        type: String,
        index: 'text'
    },
    description: {
        type: String,
        index: 'text'
    },
    price: Number,
    image: String
})

type ProductDocument = IProduct & Document
type ProductModel = IProduct & Model<ProductDocument>

export const Product = model<ProductDocument, ProductModel>("Product", productSchema)
