import { DomainError, InvalidRequestError, NotFoundError } from "../common/errors";
import { Product } from "../models/product";

export interface Product {
    _id: string;
    name: string;
    description: string;
    price: number;
    image?: string
}


export const findProducts = async (query: string = '') => {
    // return productsData.filter(p => p.name.includes(query))
    return Product.find({
        ...(query ? { $text: { $search: query } } : {})
    })
}

export const createProduct = async (product: unknown) => {
    if (!validateNewProduct(product)) { throw new InvalidRequestError('Invalid product data') }

    const saved = Product.create(product)

    return saved
}

export const updateProduct = async (product_id: string, product: unknown) => {
    if (!validateExitingProduct(product)) { throw new InvalidRequestError('Invalid product data') }

    let saved = await getProductById(product_id)
    saved.name = product.name
    saved.description = product.description
    saved.image = product.image
    await saved.update({
        // $set: {
        //     name: product.name,
        //     description: product.description,
        //     image: product.image,
        // }
    })

    return saved
}

export const getProductById = async (id: string) => {
    const product = await Product.findById(id)

    if (!product) { throw new NotFoundError('Product does not exist') }

    return product
}


export const deleteProduct = async (id: string) => {
    Product.deleteOne({
        _id: id
    })
}

function validateNewProduct(data: any): data is Product {
    return ('name' in data && 'price' in data && !('id' in data))
}
function validateExitingProduct(data: any): data is Product {
    return ('name' in data && 'price' in data && ('id' in data))
}