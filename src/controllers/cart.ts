// import { Router } from "express"
import Router from 'express-promise-router'
import { addToCart, getCart } from "../services/cart"
import { getProductById, Product } from "../services/products"

export const cartRoutes = Router()

cartRoutes.get('/', async (req, res) => {
    const cart = await getCart()
    res.send(cart)
})

cartRoutes.post('/add',
    // loadProductMiddleware(),
    async (req, res) => {
        // try {
            // req.locals.product
            const product_id = req.body.product_id

            await addToCart(product_id)
            const cart = await getCart()

            res.send(cart)
        // } catch (error) {
        //     res.status(500).send({ message: error.message })
        // }
    })
