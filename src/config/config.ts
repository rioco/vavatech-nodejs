import connectTimeout from "connect-timeout";
import cors from "cors";
import express from "express";
import session from "express-session";
import helmet from "helmet";
import morgan from "morgan";
import passport from "passport";
import { Strategy as PassportLocal } from "passport-local";
import path from "path";
import requestId from "express-request-id";
import { myUserMiddleware } from "../controllers";
import { getUserById, loginUser, User as AppUser } from "../services/users";

export function configureMiddleware(app: express.Express) {

    app.engine('jsx', require('express-react-views').createEngine());
    app.set('view engine', 'jsx');
    app.set("trust proxy", 1); // trust first proxy
    app.set('views', path.resolve(__dirname, '../../views'));

    app.use(
        helmet({
            contentSecurityPolicy: { reportOnly: true }
        }),
        cors({ credentials: true, origin: '*' }),
        requestId({}),
        // morgan(':method :url :status :res[content-length] - :response-time ms', {}),
        morgan('common', {    }),
        connectTimeout('5s', {}),
    );

    app.use(
        express.static(path.resolve(__dirname, '../../public'), {}),
        // form urlencoded <form method="POST" ...
        express.urlencoded({ extended: false }),
        // Content-Type: application/json, jQuery Ajax, XHR, fetch, ...
        express.json({}),

    )

    /* Session and Authentication */
    configurePassport()
    app.use(
        session({
            secret: "keyboard cat 123",
            resave: true,
            saveUninitialized: true,
            cookie: { secure: false, maxAge: 60000 },
        })
    )
    app.use(passport.initialize());
    app.use(passport.session());

    app.use(myUserMiddleware())
}


declare global {
    namespace Express {
        // tslint:disable-next-line:no-empty-interface
        interface AuthInfo { }
        // tslint:disable-next-line:no-empty-interface
        interface User extends AppUser { }
    }
}

export function configurePassport() {

    passport.serializeUser(function (user, done) {
        // done(null, user);
        done(null, user.id);
    });

    passport.deserializeUser(async function (id: Express.User['id'], done) {
        try {
            const user = await getUserById(id)
            done(null, user);
        } catch (err) {
            done(err);
        }
    });

    passport.use(new PassportLocal({
        usernameField: 'username',
        passwordField: 'password'
    }, async (username, password, done) => {
        /* Verify username and password */
        try {
            const user = await loginUser({ username, password })
            done(null, user)
        } catch (error) {
            done(null, false, error)
        }
    }))
}