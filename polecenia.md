# GIT
cd ..
git clone https://bitbucket.org/ev45ive/vavatech-nodejs.git vavatech-nodejs
cd vavatech-nodejs
npm i 
npm run dev

## Pobieranie zmian
git stash -u
git pull 

# Instalacje
node -v 
v14.16.1
https://nodejs.org/en/

npm -v 
6.14.6

git --version
git version 2.31.1.windows.1
https://git-scm.com/download/win

docker -v 
Docker version 20.10.6, build 370c289
https://docs.docker.com/docker-for-windows/install/
docker run -d -p 80:80 docker/getting-started
docker ps
http://localhost/tutorial/



code -v 
1.56.2

chrome://version

## Vs Code
Ctrl + ` - show hide terminal
Ctrl + Shift + ` - new terminal 

## VS Code Extensions 
https://marketplace.visualstudio.com/items?itemName=humao.rest-client

## Init
npm init
npm init -y

## NPM Packages
npm install express
npm i express

https://semver.org/
https://docs.npmjs.com/about-semantic-versioning
https://semver.npmjs.com/

<!-- Restore updated node_modules from package.json -->
npm i 
<!-- Restore exact node_modules from package-lock.json -->
npm ci 

npm outdated
npm update express
npm install express@^4.10.8
npm install express@latest

## Main file, scripts
  "main": "examples/express.js",
  PORT=9000 node .

## Env variables
cross-env PORT=8080 node ./examples/express.js

https://www.npmjs.com/package/cross-env
https://www.npmjs.com/package/dotenv

## Global packages
npm i -g typescript nodemon
<!-- C:\Users\PC\AppData\Roaming\npm\nodemon -> C:\Users\PC\AppData\Roaming\npm\node_modules\nodemon\bin\nodemon.js
C:\Users\PC\AppData\Roaming\npm\tsc -> C:\Users\PC\AppData\Roaming\npm\node_modules\typescript\bin\tsc -->

## TypeScript
tsc ./examples/ts/test.ts
tsc --watch ./examples/ts/test.ts

npm i --save-dev @types/node
tsc --init
tsc --module ESNext --lib es2020 --strict --init

## Express
npm i --save express
npm i --save-dev @types/express

## Typescript + Nodemon
npm i typescript ts-node

npx ts-node --show-config

## Debugger
npm run build

node --inspect dist/index.js 
node --inspect-brk dist/index.js 

debugger;

Debugger listening on ws://127.0.0.1:9229/d7984979-7827-4025-879c-9170b21c0471
For help, see: https://nodejs.org/en/docs/inspector
Listening on http://localhost:8080/

## Source maps
"sourceMap": true, /* Generates corresponding '.map' file. */

node --enable-source-maps --inspect-brk ./dist

nodemon 
--watch \"src/**\" 
--ext \"ts\" 
--exec \"node --inspect --enable-source-maps -r ts-node/register ./src/index.ts \" 

## Template rendering systems
https://expressjs.com/en/resources/template-engines.html

npm i ejs 
npm i pug 
npm i express-react-views react react-dom 
npm i express-hbs


## Middleware
https://expressjs.com/en/resources/middleware.html

npm i cors @types/cors express-request-id @types/express-request-id  express-session @types/express-session  morgan @types/morgan multer @types/multer response-time connect-timeout  @types/connect-timeout

### Cors
<!-- bedac na innej domene: -->
fetch('http://localhost:8080/products/1234')

api.html#req.fresh:1 Access to fetch at 'http://localhost:8080/products/1234' from origin 'https://expressjs.com' has been blocked by CORS policy: No 'Access-Control-Allow-Origin' header is present on the requested resource. If an opaque response serves your needs, set the request's mode to 'no-cors' to fetch the resource with CORS disabled.

npm i cors

## Logowanie
https://www.npmjs.com/package/morgan
https://www.npmjs.com/package/express-winston

## Helmet
https://github.com/helmetjs/helmet
https://www.npmjs.com/package/helmet-csp
https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/script-src

[Report Only] Refused to execute inline script because it violates the following Content Security Policy directive: "script-src 'self'". Either the 'unsafe-inline' keyword, a hash ('sha256-CPmUN4/8iDC7UywrX78Ii3Fh3kYMK7YlnEMSmY5oHy8='), or a nonce ('nonce-...') is required to enable inline execution.

products:1 [Report Only] Refused to load the script 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js' because it violates the following Content Security Policy directive: "script-src 'self'". Note that 'script-src-elem' was not explicitly set, so 'script-src' is used as a fallback.

## Session
node_modules\@types\express-session\index.d.ts
https://www.typescriptlang.org/docs/handbook/declaration-merging.html
```ts
declare module 'express-session' {
    interface SessionData {
        views: number;
    }
}
```

## Authentication - PassportJS
npm install passport passport-local @types/passport @types/passport-local


## File upload
bus boy https://www.npmjs.com/package/busboy

## Testing
https://zellwk.com/blog/endpoint-testing/


## Mongodb
<!-- docker run --rm -p 27017:27017 --name db  -e MONGO_INITDB_ROOT_USERNAME=root -e MONGO_INITDB_ROOT_PASSWORD=example mongo -->
docker rm db -f
docker run --rm -p 27017:27017 --name db -e MONGO_INITDB_ROOT_USERNAME=root -e MONGO_INITDB_ROOT_PASSWORD=password -e MONGODB_USERNAME=user -e MONGODB_PASSWORD=password -e MONGODB_DATABASE=database  mongo:4.4.6

docker rm db -f
docker run --rm -p 27017:27017 --name db mongo

docker exec -it db mongo database

## DEploy
<!-- Node.JS2021%$ -->
http://nodejs.server029028.nazwa.pl/

<!-- Copy package-lock, npm ci -->
scp -r ./package*.json server029028@server029028.nazwa.pl:/home/server029028/ftp/app/

<!-- Copy generated / compiledsources  -->
scp -r ./dist server029028@server029028.nazwa.pl:/home/server029028/ftp/app/dist

<!-- Switch config to production -->
scp -r ./dist/config.prod.js server029028@server029028.nazwa.pl:/home/server029028/ftp/app/dist/config.js

<!-- Copy resouces -->
scp -r ./views server029028@server029028.nazwa.pl:/home/server029028/ftp/app/views
scp -r ./public server029028@server029028.nazwa.pl:/home/server029028/ftp/app/public
scp -r ./data server029028@server029028.nazwa.pl:/home/server029028/ftp/app/data

nazwa-cdn purge
nodecli restart
tail -f -n 20 /tmp/apache_passenger.log


npm run build 
scp -r ./dist server029028@server029028.nazwa.pl:/home/server029028/ftp/app/dist
scp -r ./dist/config.prod.js server029028@server029028.nazwa.pl:/home/server029028/ftp/app/dist/config.js
ssh server029028@server029028.nazwa.pl nazwa-cdn purge; nodecli restart
