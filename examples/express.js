
import express from 'express'
import { calculateResult, /* secret */} from './lib.js'
import getResult from './lib.js'

const hostname = process.env.HOST || '127.0.0.1'
const port = process.env.PORT

const server = express()
server.get('/', (req,res)=>{
    const result = getResult()
    res.send(`
        secret : ${secret} \n
        ${result} \n
        Hello NodeJS with Nodemon and ExpressJS!
    `)
})

// const server = http.createServer((req, res) => {

//     res.statusCode = 200
//     res.setHeader('Content-Type', 'text/plain')

//     res.write('secret : ' + lib.secret + '\n ')
//     const result = lib.calculateResult()
//     res.write(result)
//     res.end('Hello World!\n ')
// })

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`)
})