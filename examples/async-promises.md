```js
function echo(msg, callback){

    setTimeout(()=> {
        callback(null, msg)
    }, 2000)
}

var costam = ' cos tam jeszcze'

echo('Ala', function(error, user){
    if(error){ user = 'Nieznany user ' }

    echo(user + ' ma kota', function(error, result){        
        if(error){ return; }

        console.log(result + ' i ' +  costam)
    })
})
```

```js
function echo(msg){
    return new Promise((resolve)=>{

        setTimeout(()=> {
            resolve(msg)
        }, 2000)
    }) 
}
i = 0;
counter = setInterval(() => console.log(++i),1000)

var costam = ' cos tam jeszcze'

promise = echo('Ala') 

p2 = promise.then((user) => user + ' ma ')
p3 = p2.then((res) => {
    return echo(res  + 'kota')
})

p3.then( res => {
    console.log(res)
    clearInterval(counter)
}) 
```

## Error handling
```js
function echo(msg,err){
    return new Promise((resolve,reject)=>{

        setTimeout(()=> {
            if(err) { reject(err) } else { resolve(msg) }
        }, 2000)
    }) 
}
i = 0;
counter = setInterval(() => console.log(++i),1000)

var costam = ' cos tam jeszcze'

promise = echo('Ala'/*,'upss..'*/) 
p2 = promise.then((user) => user + ' ma ', err => 'Nikt nie ma ')

p3A = p2.then(res => console.log('User zalogowany ' + res) )


p3B = p2.then((res) => {
    return echo(res  + 'kota' /*, 'upss 2..'*/)
})
.catch((err)=>{
    console.log('Error ocurred',err)
    return Promise.reject('Nie ma danych o kocie')
})
.then( res => { console.log(res)},err => console.error(err))
.finally(() => clearInterval(counter))
```